//
//  main.m
//  SingleView
//
//  Created by Danny Draper on 04/01/2014.
//  Copyright (c) 2014 Danny Draper. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SingleViewAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SingleViewAppDelegate class]));
    }
}
