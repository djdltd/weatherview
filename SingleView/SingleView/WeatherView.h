//
//  WeatherView.h
//  SingleView
//
//  Created by Danny Draper on 08/01/2014.
//  Copyright (c) 2014 Danny Draper. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeatherView : UIView {

    UIImage *_largenumbers;
    NSMutableArray *_numberframes;
    UIImage *_frame;
    NSMutableString *_curtempstring;
}

- (void) setTempstring:(NSString *)tempstring;
- (void) PaintString;

@end
