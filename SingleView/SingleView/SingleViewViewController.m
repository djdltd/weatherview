//
//  SingleViewViewController.m
//  SingleView
//
//  Created by Danny Draper on 04/01/2014.
//  Copyright (c) 2014 Danny Draper. All rights reserved.
//

#import "SingleViewViewController.h"

@interface SingleViewViewController ()

@end

@implementation SingleViewViewController

@synthesize btn_testbutton;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // To perform a WOEID Query:
    // http://where.yahooapis.com/v1/places.q('Brentwood,%20Nashville')?lang=en&appid=aCmcXHzV34H3vtt4nmaSxd.xxp2Gkdr41k84JLkaucA_kgX_xfVTdI9APQXB6g--
    
    _feeds = [[NSMutableArray alloc] init];
    
    NSURL *url = [NSURL URLWithString:@"http://weather.yahooapis.com/forecastrss?w=28835638&u=c"];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"sunny-day-wallpaper.jpg"]]];
    
    _parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    
    [_parser setDelegate:self];
    [_parser setShouldResolveExternalEntities:NO];
    [_parser parse];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnTestbuttonreleased:(id)sender
{
    NSLog(@"This is a test button press");
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"cloudy_day.jpg"]]];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    _element = elementName;
    
    if ([_element isEqualToString:@"yweather:condition"]) {
        
        NSLog(_element);
        
        NSEnumerator *enumerator =  [attributeDict keyEnumerator];
        NSString *key;
        
        while (key = [enumerator nextObject]) {
            NSLog(key);
            NSLog([attributeDict valueForKey:key]);
            
        }
        
        
        
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if ([_element isEqualToString:@"yweather:condition"]) {
        NSLog(string);
    }
    
    
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
}

- (void)drawRect:(CGRect)rect {
    NSLog (@"Draw rect was called!");
}

@end
