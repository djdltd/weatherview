//
//  WeatherView.m
//  SingleView
//
//  Created by Danny Draper on 08/01/2014.
//  Copyright (c) 2014 Danny Draper. All rights reserved.
//

#import "WeatherView.h"

@implementation WeatherView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder
{
	if ((self = [super initWithCoder:coder])) {
    
        CGFloat startx = 10.0f;
        CGFloat starty = 9.0f;
        
        CGFloat yoffset = 0.0f;
        CGFloat xoffset = 0.0f;
        
        if (_largenumbers == nil)
		{
			_largenumbers = [UIImage imageNamed:@"WeatherNumbers.png"];
		}
        
        _numberframes = [NSMutableArray new];
        
        
        CGRect imageRect;
        UIImage *frame;
        
        int i = 0;
        
        // First level of numbers
        for (i=0;i<10;++i)
        {
            imageRect.origin = CGPointMake (startx + xoffset, starty + yoffset);
            imageRect.size = CGSizeMake(49.0f, 109.0f);
            frame = [UIImage imageWithCGImage:CGImageCreateWithImageInRect([_largenumbers CGImage], imageRect)];
            
            [_numberframes addObject:frame];
            
            xoffset+=49.0f;
        }
    }
    
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    //[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
    
    [_largenumbers drawAtPoint:CGPointMake(10, 10)];
    // Drawing code
    NSLog(@"The UIView was painted");
}

- (void) setTempstring:(NSString *)tempstring
{
	[_curtempstring setString:tempstring];
	
}

- (void) PaintString
{
	int c = 0;
	
	CGFloat xoffset = 0.0f;
	CGFloat xincrement = 49.0f;
	
	CGFloat xlocation = 0.0f;
	CGFloat ylocation = 0.0f;
	
	for (c=0;c<[_curtempstring length];++c)
	{
		UniChar singlechar = [_curtempstring characterAtIndex:c];
		
		
		if (singlechar == '0') {
			_frame = [_numberframes objectAtIndex:0];
			[_frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;
		}
		
		if (singlechar == '1') {
			_frame = [_numberframes objectAtIndex:1];
			[_frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;
		}
		
		if (singlechar == '2') {
			_frame = [_numberframes objectAtIndex:2];
			[_frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;
		}
		
		if (singlechar == '3') {
			_frame = [_numberframes objectAtIndex:3];
			[_frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;
		}
		
		if (singlechar == '4') {
			_frame = [_numberframes objectAtIndex:4];
			[_frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;
		}
		
		if (singlechar == '5') {
			_frame = [_numberframes objectAtIndex:5];
			[_frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;
		}
		
		if (singlechar == '6') {
			_frame = [_numberframes objectAtIndex:6];
			[_frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;
		}
		
		if (singlechar == '7') {
			_frame = [_numberframes objectAtIndex:7];
			[_frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;
		}
		
		if (singlechar == '8') {
			_frame = [_numberframes objectAtIndex:8];
			[_frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;
		}
		
		if (singlechar == '9') {
			_frame = [_numberframes objectAtIndex:9];
			[_frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;
		}
	}
}


@end
