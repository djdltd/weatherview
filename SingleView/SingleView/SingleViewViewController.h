//
//  SingleViewViewController.h
//  SingleView
//
//  Created by Danny Draper on 04/01/2014.
//  Copyright (c) 2014 Danny Draper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WeatherView.h"



@interface SingleViewViewController : UIViewController <NSXMLParserDelegate> {

    IBOutlet UIButton *btn_testbutton;    
    IBOutlet WeatherView *_weatherview;
    
    NSMutableArray *_feeds;
    NSXMLParser *_parser;
    NSString *_element;
}

@property (retain, nonatomic) UIButton *btn_testbutton;

- (IBAction)btnTestbuttonreleased:(id)sender;

@end

