//
//  SingleViewAppDelegate.h
//  SingleView
//
//  Created by Danny Draper on 04/01/2014.
//  Copyright (c) 2014 Danny Draper. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SingleViewAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;



@end
